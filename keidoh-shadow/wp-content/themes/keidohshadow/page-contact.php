<?php get_header(); ?>

<div id="page-contact">
	<div id="wrap">
		<div id="page-title-panel">
			<h1 id="page-title">CONTACT</h1>
		</div>
		<div id="form-panel">
			<?php
            // 最後に追加されたメールデータが表示されます
            $post_type = esc_sql('mw-wp-form');
            $post_status = esc_sql('publish');
            $query = "SELECT * FROM $wpdb->posts WHERE post_status='$post_status' and post_type = '$post_type' ORDER BY ID desc;";
            $results = $wpdb->get_results($query);
            count($results)==0?esc_html_e('mw wp formにメール設定を追加してください。'):'';
            foreach($results as $row) {
                $id = $row->ID; // 結果データの型はオブジェクト
                echo do_shortcode("[mwform_formkey key=\"$id\"]");
                break;
            }


            ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>