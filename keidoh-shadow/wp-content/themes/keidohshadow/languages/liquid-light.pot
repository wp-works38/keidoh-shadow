# WordPress Blank Pot
# Copyright (C) 2014 ...
# This file is distributed under the GNU General Public License v2 or later.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: "
"liquid-light\n"
"POT-Creation-Date: "
"2017-07-14 20:15+0900\n"
"PO-Revision-Date: \n"
"Last-Translator: Your "
"Name <you@example.com>\n"
"Language-Team: LIQUID "
"PRESS <info@lqd.jp>\n"
"Report-Msgid-Bugs-To: "
"Translator Name "
"<translations@example."
"com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/"
"plain; charset=UTF-8\n"
"Content-Transfer-"
"Encoding: 8bit\n"
"Plural-Forms: "
"nplurals=2; plural=n != "
"1;\n"
"X-Textdomain-Support: "
"yesX-Generator: Poedit "
"1.6.4\n"
"X-Poedit-SourceCharset: "
"UTF-8\n"
"X-Poedit-KeywordsList: "
"__;_e;esc_html_e;"
"esc_html_x:1,2c;"
"esc_html__;esc_attr_e;"
"esc_attr_x:1,2c;"
"esc_attr__;_ex:1,2c;"
"_nx:4c,1,2;"
"_nx_noop:4c,1,2;_x:1,2c;"
"_n:1,2;_n_noop:1,2;"
"__ngettext:1,2;"
"__ngettext_noop:1,2;_c,"
"_nc:4c,1,2\n"
"X-Poedit-Basepath: ..\n"
"Language: en_US\n"
"X-Generator: Poedit "
"2.0.1\n"
"X-Poedit-"
"SearchPath-0: .\n"

#: 404.php:6
msgid "Not found."
msgstr ""

#: archive.php:65
#: index.php:154
#: page.php:38
#: search.php:61
#: single.php:63
#: single.php:154
msgid "No articles"
msgstr ""

#: comments.php:12
#, php-format
msgid "Comment (%1$s)"
msgid_plural ""
"Comments (%1$s)"
msgstr[0] ""
msgstr[1] ""

#: footer.php:48
msgid "(C)"
msgstr ""

#: footer.php:48
msgid ""
". All rights reserved."
msgstr ""

#: footer.php:51
msgid "Theme by"
msgstr ""

#: footer.php:51
msgid ""
"Responsive WordPress "
"Theme LIQUID PRESS"
msgstr ""

#: footer.php:51
msgid "LIQUID PRESS"
msgstr ""

#: functions.php:700
#: functions.php:789
msgid "Title:"
msgstr ""

#: functions.php:706
msgid "Facebook Page URL:"
msgstr ""

#: functions.php:792
msgid ""
"Number of posts to show:"
msgstr ""

#: functions.php:929
#: single.php:74
msgid "&laquo; Prev"
msgstr ""

#: functions.php:930
#: single.php:82
msgid "Next &raquo;"
msgstr ""

#: header.php:24
msgid "TOP"
msgstr ""

#: header.php:197
msgid "Toggle navigation"
msgstr ""

#: search.php:8
#, php-format
msgid "Search: %s"
msgstr ""

#: share.php:2
msgid "Facebook"
msgstr ""

#: share.php:3
msgid "Twitter"
msgstr ""

#: share.php:4
msgid "Google+"
msgstr ""

#: share.php:5
msgid "Hatena"
msgstr ""

#: sidebar.php:7
msgid "Categories"
msgstr ""

#: sidebar.php:13
msgid "Tags"
msgstr ""

#: single.php:89
msgid "Recommend"
msgstr ""
