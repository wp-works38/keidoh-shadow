<?php
/*
Author: LIQUID DESIGN Ltd.
Author URI: https://lqd.jp/
*/

// ------------------------------------
// scripts and styles
// ------------------------------------
$liquid_theme = wp_get_theme();
function liquid_scripts_styles() {
    global $liquid_theme;
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), $liquid_theme->Version );
    wp_enqueue_style( 'icomoon', get_template_directory_uri() . '/css/icomoon.css', array() );
    wp_enqueue_style( 'liquid-style', get_stylesheet_uri(), array(), $liquid_theme->Version );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), $liquid_theme->Version );
    wp_enqueue_script( 'liquid-script', get_template_directory_uri() . '/js/common.min.js', array( 'jquery' ), $liquid_theme->Version );
    if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
}
add_action( 'wp_enqueue_scripts', 'liquid_scripts_styles' );

// get_the_archive_title
if ( ! function_exists( 'liquid_custom_archive_title' ) ) :
function liquid_custom_archive_title( $title ){
    if ( is_category() ) {
        $title = single_term_title( '', false );
    }
    return $title;
}
add_filter( 'get_the_archive_title', 'liquid_custom_archive_title', 10 );
endif;

// title
add_filter( 'wp_title', 'liquid_wp_title', 10, 2 );
function liquid_wp_title( $title, $sep ) {
	global $paged, $page;
	if ( is_feed() )
		return $title;
	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );
	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";
	return $title;
}

// body_class
add_filter('body_class', 'liquid_class_names');
function liquid_class_names($classes) {
    if (is_single()){
        $cat = get_the_category();
        if(!empty($cat)){
            $parent_cat_id = $cat[0]->parent;
            if(empty($parent_cat_id)){
                $parent_cat_id = $cat[0]->cat_ID;
            }
        }else{
            $parent_cat_id = "0";
        }    
        $classes[] = "category_".$parent_cat_id;
    }
    if (is_page()){
        $page = get_post( get_the_ID() );
        $slug = $page->post_name;
        $classes[] = "page_".$slug;
    }
	return $classes;
}

// set_post_thumbnail_size(220, 165, true ); // 幅、高さ、トリミング

// カテゴリ説明でHTML使用可能
remove_filter( 'pre_term_description', 'wp_filter_kses' );

// Remove p tags from category description
remove_filter('term_description','wpautop');

// excerpt_more
function liquid_new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'liquid_new_excerpt_more');

// after_setup_theme
if ( ! function_exists( 'liquid_after_setup_theme' ) ) :
function liquid_after_setup_theme() {
    add_theme_support( 'custom-background' );
    add_theme_support( 'title-tag' );
    // アイキャッチ画像、投稿とコメントのRSSフィード
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    // editor-style
    add_editor_style( 'css/editor-style.css' );
    // Set
    if ( ! isset( $content_width ) ) $content_width = 1024;
    add_theme_support( 'customize-selective-refresh-widgets' );
    // nav_menu
    register_nav_menus(array(
        'global-menu' => ('Global Menu')
    ));
    // 固定ページの抜粋対応
    add_post_type_support( 'page', 'excerpt' );
    // languages
    load_theme_textdomain( 'liquid-light', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'liquid_after_setup_theme' );
endif;

// no_self_ping
function liquid_no_self_ping( &$links ) {
    $home = home_url();
    foreach ( $links as $l => $link )
    if ( 0 === strpos( $link, $home ) )
    unset($links[$l]);
}
add_action( 'pre_ping', 'liquid_no_self_ping' );

// ------------------------------------
// カスタムヘッダーの設定
// ------------------------------------
$defaults = array(
	'default-image'          => get_template_directory_uri().'/images/logo.png',
	'random-default'         => false,
	'width'                  => 400,
	'height'                 => 72,
	'flex-height'            => true,
	'flex-width'             => false,
	'default-text-color'     => '333333',
	'header-text'            => false,
	'uploads'                => true,
	'admin-preview-callback' => 'liquid_admin_header_image',
	'admin-head-callback'    => 'liquid_admin_header_style',
);

function liquid_admin_header_image() {
?>
	<p class="header_preview"><?php bloginfo('description'); ?></p>
	<?php if(get_header_image()): ?>
		<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php bloginfo('name'); ?>" />
	<?php else : ?>
		<h2 class="header_preview"><?php bloginfo('name'); ?></h2>
	<?php endif; ?>
<?php
}

function liquid_admin_header_style() {
?>
<style type="text/css">
p.header_preview,h2.header_preview {
	color:#<?php echo get_header_textcolor(); ?>;
}
</style>
<?php
}
add_theme_support( 'custom-header', $defaults );

// インラインスタイル削除
function liquid_remove_recent_comments_style() {
    global $wp_widget_factory;
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}
add_action('widgets_init', 'liquid_remove_recent_comments_style');

// generator削除
remove_action('wp_head', 'wp_generator');

// ------------------------------------
// カスタマイザーで設定する項目を追加
// ------------------------------------
if ( ! function_exists( 'liquid_theme_customize_register' ) ) :
function liquid_theme_customize_register($lqd_customize) {
    //ショートカット
	//$lqd_customize->get_setting( 'blogname' )->transport          = 'postMessage';
	$lqd_customize->get_setting( 'blogdescription' )->transport   = 'postMessage';
    if ( isset( $lqd_customize->selective_refresh ) ) {
        //$lqd_customize->selective_refresh->add_partial('blogname', array(
        //    'selector' => '.headline .logo',
        //    'render_callback' => function() { bloginfo('name'); },
        //));
        $lqd_customize->selective_refresh->add_partial('blogdescription', array(
            'selector' => '.headline .subttl',
            //'render_callback' => function() { bloginfo('description'); },
        ));
        $lqd_customize->selective_refresh->add_partial('img_options[img01]', array(
            'selector' => '.cover img',
        ));
        $lqd_customize->selective_refresh->add_partial('text_options[text01]', array(
            'selector' => '.cover .main',
        ));
        $lqd_customize->selective_refresh->add_partial('sns_options[facebook]', array(
            'selector' => '.sns',
        ));
    }
    //テキストカラー
	$lqd_customize->add_setting( 'color_options[color]', array(
        'default' => '#333333',
		'type'    => 'option',
	));
	$lqd_customize->add_control( new WP_Customize_Color_Control(
		$lqd_customize, 'color_options[color]',
		array(
			'label' => 'テキストカラー',
			'section' => 'colors',
			'settings' => 'color_options[color]'
	)));
    //テーマカラー
	$lqd_customize->add_setting( 'color_options[color2]', array(
        'default' => '#00AEEF',
		'type'    => 'option'
	));
	$lqd_customize->add_control( new WP_Customize_Color_Control(
		$lqd_customize, 'color_options[color2]',
		array(
			'label' => 'テーマカラー',
			'section' => 'colors',
			'settings' => 'color_options[color2]'
	)));
    
    //レイアウト
    $lqd_customize->add_section('col_sections', array(
        'title'    => 'レイアウト',
        'priority' => 102,
    ));
    $lqd_customize->add_setting('col_options[sidebar]', array(
        'type' => 'option',
        'default' => '0',
    ));
    $lqd_customize->add_control('col_options[sidebar]', array(
        'label'      => '投稿ページのレイアウト',
        'description'=> '投稿ページでプレビューできます。<br><img src="'.get_template_directory_uri().'/images/col.png" alt="カラム">',
        'section'    => 'col_sections',
        'settings'   => 'col_options[sidebar]',
        'type'     => 'radio',
		'choices'  => array(
			'0' => '2カラム',
			'1'  => '1カラム',
		),
    ));
    $lqd_customize->add_setting('col_options[_recommend]', array(
        'type' => 'option',
    ));
    $lqd_customize->add_control('col_options[_recommend]', array(
        'label'      => 'LIQUID PRESS について',
        'description'=> 'MAGAZINEテーマではカラムレイアウトのカスタマイズや人気記事の設定、CORPORATEテーマでは会社情報や多言語対応の設定等が可能です。<br>詳しくは<a href="https://lqd.jp/wp/?utm_source=admin&utm_medium=lead&utm_campaign=trial" target="_blank">こちら</a>をご覧ください。',
        'section'    => 'col_sections',
        'settings'   => 'col_options[_recommend]',
        'type'     => 'hidden',
    ));
    
    //テキスト
    $lqd_customize->add_section('text_sections', array(
        'title'    => 'スライドショーコピー',
        'priority' => 101,
        'description'=> 'スライドショー画像の下に表示されます。全て空にするとエリアが非表示になります。&lt;a href="xxxx"&gt;テキスト&lt;/a&gt;でリンクになります。',
    ));
    $lqd_customize->add_setting('text_options[text01]', array(
        'type'           => 'option',
    ));
    $lqd_customize->add_control('text_options[text01]', array(
        'label'      => 'スライドショーコピー1',
        'section'    => 'text_sections',
        'settings'   => 'text_options[text01]',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('text_options[text02]', array(
        'type'           => 'option',
    ));
    $lqd_customize->add_control('text_options[text02]', array(
        'label'      => 'スライドショーコピー2',
        'section'    => 'text_sections',
        'settings'   => 'text_options[text02]',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('text_options[text03]', array(
        'type'           => 'option',
    ));
    $lqd_customize->add_control('text_options[text03]', array(
        'label'      => 'スライドショーコピー3',
        'section'    => 'text_sections',
        'settings'   => 'text_options[text03]',
        'type'       => 'text',
    ));
    
    //SEO
    $lqd_customize->add_section('seo_sections', array(
        'title'    => 'SEO',
        'description'=> 'SEO対策関連の設定をします。外部プラグインを併用する場合は重複防止のため表示しないに設定してください。',
        'priority' => 106,
    ));
    $lqd_customize->add_setting('seo_options[meta]', array(
        'type' => 'option',
        'default' => '0',
    ));
    $lqd_customize->add_control('seo_options[meta]', array(
        'label'      => 'META Description、Author、Next、Prev自動表示',
        'settings'   => 'seo_options[meta]',
        'section'    => 'seo_sections',
        'type'     => 'select',
		'choices'  => array(
			'0' => 'する',
			'1'  => 'しない',
		),
    ));
    $lqd_customize->add_setting('seo_options[ogp]', array(
        'type' => 'option',
        'default' => '0',
    ));
    $lqd_customize->add_control('seo_options[ogp]', array(
        'label'      => 'OGP、TwitterCards自動表示',
        'settings'   => 'seo_options[ogp]',
        'section'    => 'seo_sections',
        'type'     => 'select',
		'choices'  => array(
			'0' => 'する',
			'1'  => 'しない',
		),
    ));
    
    //SNS
    $lqd_customize->add_section('sns_sections', array(
        'title'    => 'SNSアカウント',
        'description'=> 'URLを入力するとアイコンなどが表示されます。',
        'priority' => 108,
    ));
    $lqd_customize->add_setting('sns_options[facebook]', array('type' => 'option',));
    $lqd_customize->add_control('sns_options[facebook]', array(
        'label'      => 'Facebook URL',
        'description'=> '例：https://www.facebook.com/lqdjp',
        'settings'   => 'sns_options[facebook]',
        'section'    => 'sns_sections',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('sns_options[twitter]', array('type' => 'option',));
    $lqd_customize->add_control('sns_options[twitter]', array(
        'label'      => 'Twitter URL',
        'settings'   => 'sns_options[twitter]',
        'section'    => 'sns_sections',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('sns_options[google-plus]', array('type' => 'option',));
    $lqd_customize->add_control('sns_options[google-plus]', array(
        'label'      => 'Google-plus URL',
        'settings'   => 'sns_options[google-plus]',
        'section'    => 'sns_sections',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('sns_options[tumblr]', array('type' => 'option',));
    $lqd_customize->add_control('sns_options[tumblr]', array(
        'label'      => 'Tumblr URL',
        'settings'   => 'sns_options[tumblr]',
        'section'    => 'sns_sections',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('sns_options[instagram]', array('type' => 'option',));
    $lqd_customize->add_control('sns_options[instagram]', array(
        'label'      => 'Instagram URL',
        'settings'   => 'sns_options[instagram]',
        'section'    => 'sns_sections',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('sns_options[youtube]', array('type' => 'option',));
    $lqd_customize->add_control('sns_options[youtube]', array(
        'label'      => 'YouTube URL',
        'settings'   => 'sns_options[youtube]',
        'section'    => 'sns_sections',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('sns_options[flickr]', array('type' => 'option',));
    $lqd_customize->add_control('sns_options[flickr]', array(
        'label'      => 'Flickr URL',
        'settings'   => 'sns_options[flickr]',
        'section'    => 'sns_sections',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('sns_options[pinterest]', array('type' => 'option',));
    $lqd_customize->add_control('sns_options[pinterest]', array(
        'label'      => 'Pinterest URL',
        'settings'   => 'sns_options[pinterest]',
        'section'    => 'sns_sections',
        'type'       => 'text',
    ));
    $lqd_customize->add_setting('sns_options[feed]', array('type' => 'option',));
    $lqd_customize->add_control('sns_options[feed]', array(
        'label'      => 'Feed アイコン表示',
        'settings'   => 'sns_options[feed]',
        'section'    => 'sns_sections',
        'type'     => 'select',
		'choices'  => array(
			'0' => 'する',
			'1'  => 'しない',
		),
    ));
        
    //GA
    $lqd_customize->add_section('html_sections', array(
        'title'    => 'Googleアナリティクス',
        'description'=> 'Googleアナリティクスのアナリティクス設定＞プロパティ設定でトラッキングIDを確認できます。',
        'priority' => 109,
    ));
    $lqd_customize->add_setting('html_options[ga]', array('type'  => 'option',));
    $lqd_customize->add_control('html_options[ga]', array(
        'label'      => 'トラッキングID',
        'description'=> '例：UA-XXXXXXX-XX',
        'section'    => 'html_sections',
        'settings'   => 'html_options[ga]',
        'type'       => 'text',
    ));
    
    //スライドショー画像
    $lqd_customize->add_section( 'img_sections' , array(
        'title'        => 'スライドショー画像/動画',
        'description'  => '画像を3枚まで設定できます。1枚のみ固定表示も可。全て空にするとエリアが非表示になります。画像は横縦2:1位がおすすめです。動画を設定した場合はスライドしません。スマホでは動画ではなくスライドショー画像が表示されます。',
        'priority'     => 100,
    ));
    $lqd_customize->add_setting( 'img_options[img01]' );
    $lqd_customize->add_control( new WP_Customize_Image_Control(
        $lqd_customize, 'img_options[img01]', array(
            'label' => '画像1',
            'section' => 'img_sections',
            'settings' => 'img_options[img01]',
            'description' => '画像をアップロード',
    )));
    $lqd_customize->add_setting( 'img_options[img02]' );
    $lqd_customize->add_control( new WP_Customize_Image_Control(
        $lqd_customize, 'img_options[img02]', array(
            'label' => '画像2',
            'section' => 'img_sections',
            'settings' => 'img_options[img02]',
            'description' => '画像をアップロード',
    )));
    $lqd_customize->add_setting( 'img_options[img03]' );
    $lqd_customize->add_control( new WP_Customize_Image_Control(
        $lqd_customize, 'img_options[img03]', array(
            'label' => '画像3',
            'section' => 'img_sections',
            'settings' => 'img_options[img03]',
            'description' => '画像をアップロード',
    )));
    //動画
    $lqd_customize->add_setting( 'video_options[mp4]' );
    $lqd_customize->add_control( new WP_Customize_Upload_Control(
        $lqd_customize, 'video_options[mp4]', array(
            'label' => '動画（MP4/WebM）',
            'section' => 'img_sections',
            'settings' => 'video_options[mp4]',
            'description' => '動画をアップロード',
    )));
    $lqd_customize->add_setting('video_options[url]', array(
        'type'           => 'option',
    ));
    $lqd_customize->add_control('video_options[url]', array(
        'label'      => '動画（YouTube URL）',
        'section'    => 'img_sections',
        'settings'   => 'video_options[url]',
        'type'       => 'text',
    ));
    
    //オプション
    $lqd_customize->add_section('opt_sections', array(
        'title'    => 'オプション',
        'description'=> '【上級者向け】オプション機能は導入サポート対象外とさせて頂いております。',
        'priority' => 900,
    ));
    //viewport
    $lqd_customize->add_setting('col_options[viewport]', array(
        'type' => 'option',
    ));
    $lqd_customize->add_control('col_options[viewport]', array(
        'label'      => 'Viewportカスタマイズ',
        'description'=> 'Viewportタグを変更することができます。<br>例：&lt;meta name="viewport" content="..."&gt;',
        'section'    => 'opt_sections',
        'settings'   => 'col_options[viewport]',
        'type'     => 'textarea',
    ));
    //nav
    $lqd_customize->add_setting('col_options[dropdown]', array(
        'type' => 'option',
        'default' => '0',
    ));
    $lqd_customize->add_control('col_options[dropdown]', array(
        'label'      => 'ドロップダウンメニュー',
        'description'=> 'メニューの副項目を全ての端末でドロップダウンにします。デフォルトは「しない」です。',
        'section'    => 'opt_sections',
        'settings'   => 'col_options[dropdown]',
        'type'     => 'select',
		'choices'  => array(
			'1' => 'する',
			'0'  => 'しない',
		),
    ));
    //widget
    $lqd_customize->add_setting('col_options[widget]', array(
        'type' => 'option',
        'default' => '0',
    ));
    $lqd_customize->add_control('col_options[widget]', array(
        'label'      => 'ウィジェットのデバイス判定',
        'description'=> 'デバイス判定機能を追加します。デフォルトは「する」です。',
        'section'    => 'opt_sections',
        'settings'   => 'col_options[widget]',
        'type'     => 'select',
		'choices'  => array(
			'0' => 'する',
			'1'  => 'しない',
		),
    ));
    //widget_ua
    $lqd_customize->add_setting('col_options[widget_ua]', array(
        'type' => 'option',
        'default' => '0',
    ));
    $lqd_customize->add_control('col_options[widget_ua]', array(
        'label'      => 'ウィジェットのデバイス判定（UA）',
        'description'=> 'デバイス判定にUA判定を追加します。デフォルトは「しない」です。',
        'section'    => 'opt_sections',
        'settings'   => 'col_options[widget_ua]',
        'type'     => 'select',
		'choices'  => array(
			'1' => 'する',
			'0'  => 'しない',
		),
    ));
    //wpautop
    $lqd_customize->add_setting('col_options[wpautop]', array(
        'type' => 'option',
        'default' => '0',
    ));
    $lqd_customize->add_control('col_options[wpautop]', array(
        'label'      => 'タグの自動挿入',
        'description'=> '作成した記事内の改行をpタグに変換して自動挿入します。WordPressのデフォルトは「する」です。',
        'section'    => 'opt_sections',
        'settings'   => 'col_options[wpautop]',
        'type'     => 'select',
		'choices'  => array(
			'0' => 'する',
			'1'  => 'しない',
		),
    ));
    
    //ライセンス
    $lqd_customize->add_section('lqd_sections', array(
        'title'    => 'ライセンスID',
        'description'=> 'LIQUID PRESS テーマのライセンスID（メールアドレス）を入力してください。<br>ライセンスIDのご購入は<a href="https://lqd.jp/wp/theme.html?utm_source=admin&utm_medium=wp&utm_campaign=trial" target="_blank">こちら</a>。',
        'priority' => 999,
    ));
    $lqd_customize->add_setting('lqd_options[ls]', array('type'  => 'option',));
    $lqd_customize->add_control('lqd_options[ls]', array(
        'label'      => 'メールアドレス',
        'section'    => 'lqd_sections',
        'settings'   => 'lqd_options[ls]',
        'type'       => 'text',
    ));
}
add_action( 'customize_register', 'liquid_theme_customize_register' );
endif;

// ------------------------------------
// wp_nav_menu
// ------------------------------------
function liquid_special_nav_class( $classes, $item ) {
    $classes[] = 'nav-item d-none d-md-block';
    return $classes;
}
add_filter( 'nav_menu_css_class', 'liquid_special_nav_class', 10, 2 );

// ------------------------------------
// 投稿フォーマットを追加
// ------------------------------------
//add_theme_support( 'post-formats', array( 'aside' ) );

// ------------------------------------
// ウィジェットフック for LIQUID PRESS
// ------------------------------------
//項目
$col_options = get_option('col_options');
if(empty($col_options['widget'])){
    add_action( 'in_widget_form', 'liquid_widget_form', 10, 3 );
    add_filter( 'widget_update_callback', 'liquid_widget_update_callback', 10, 2 );
    if(!empty($col_options['widget_ua'])){
        add_filter( 'widget_display_callback', 'liquid_widget_display_callback', 10, 3 );
    }
    add_filter( 'dynamic_sidebar_params', 'liquid_dynamic_sidebar_params' );
}
function liquid_widget_form( $widget, $return, $instance ) {
    $liquid_devices = isset( $instance['liquid_devices'] ) ? esc_attr( $instance['liquid_devices'] ) : ''; ?>
	<hr>
	<p class="liquid_widget_form">
		<select class="liquid_devices" id="widget-<?php echo $widget->id_base; ?>-<?php echo $widget->number; ?>-liquid_devices" name="widget-<?php echo $widget->id_base; ?>[<?php echo $widget->number; ?>][liquid_devices]">
			<option value="">-- Devices --</option>
			<option value="mobile" <?php selected( 'mobile', $liquid_devices, true ); ?>>Mobile</option>
			<option value="desktop" <?php selected( 'desktop', $liquid_devices, true ); ?>>Desktop</option>
		</select>
	</p>
	<?php
	return $instance;
}
//保存
function liquid_widget_update_callback( $instance, $new_instance ) {
	$instance['liquid_devices'] = $new_instance['liquid_devices'];
	return $instance;
}
//表示
function liquid_widget_display_callback( $instance, $widget, $args ) {
    if( !empty($instance['liquid_devices']) ) {
        if( $instance['liquid_devices']=="mobile" && !wp_is_mobile() ) {
            return false;
        }elseif( $instance['liquid_devices']=="desktop" && wp_is_mobile() ) {
            return false;
        }
    }
    return $instance;
}
//CSS
function liquid_dynamic_sidebar_params( $params ) {
	global $wp_registered_widgets;

	$widget_id  = $params[0]['widget_id'];
	$widget_obj = $wp_registered_widgets[$widget_id];
	$widget_opt = get_option($widget_obj['callback'][0]->option_name);
	$widget_num = $widget_obj['params'][0]['number'];

	if ( empty($widget_opt[$widget_num]['liquid_devices']) ){
        return $params;
    }else{
        if( $widget_opt[$widget_num]['liquid_devices']=="mobile" ){
            $liquid_devices_class = "d-md-none";
        }elseif( $widget_opt[$widget_num]['liquid_devices']=="desktop" ){
            $liquid_devices_class = "d-none d-md-block";
        }
        $params[0]['before_widget'] = preg_replace( '/class="widget/', "class=\"{$liquid_devices_class} widget", $params[0]['before_widget'], 1 );
        return $params;
    }
}

// ------------------------------------
// ウィジェットの登録
// ------------------------------------
if ( ! function_exists( 'liquid_widgets_init' ) ) :
function liquid_widgets_init() {
    register_sidebar(array(
        'name' => 'サイドバー',
        'id' => 'sidebar',
        'before_title' => '<div class="ttl">',
        'after_title' => '</div>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>'
    ));
    register_sidebar(array(
        'name' => 'ヘッドライン',
        'id' => 'headline',
        'before_title' => '<div class="ttl">',
        'after_title' => '</div>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>'
    ));
    register_sidebar(array(
        'name' => 'ページヘッダー',
        'description' => '横幅100%のエリアです。',
        'id' => 'page_header',
        'before_title' => '<div class="container"><div class="ttl">',
        'after_title' => '</div></div>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>'
    ));
    register_sidebar(array(
        'name' => 'メインエリア上部',
        'id' => 'main_head',
        'before_title' => '<div class="ttl">',
        'after_title' => '</div>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>'
    ));
    register_sidebar(array(
        'name' => 'メインエリア下部',
        'id' => 'main_foot',
        'before_title' => '<div class="ttl">',
        'after_title' => '</div>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>'
    ));
    register_sidebar(array(
        'name' => 'トップページヘッダー',
        'id' => 'top_header',
        'before_title' => '<div class="ttl">',
        'after_title' => '</div>',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>'
    ));
    register_sidebar(array(
        'name' => 'ページフッター',
        'description' => '横幅100%のエリアです。',
        'id' => 'page_footer',
        'before_title' => '<div class="container"><div class="ttl">',
        'after_title' => '</div></div>',
        'before_widget' => '<div id="%1$s" class="col-sm-12"><div class="widget %2$s">',
        'after_widget'  => '</div></div>'
    ));
    register_sidebar(array(
        'name' => 'フッター',
        'description' => 'フッターは3カラムです。',
        'id' => 'footer',
        'before_title' => '<div class="ttl">',
        'after_title' => '</div>',
        'before_widget' => '<div id="%1$s" class="widget %2$s col-sm-4">',
        'after_widget'  => '</div>'
    ));
}
add_action( 'widgets_init', 'liquid_widgets_init' );
endif;

// ------------------------------------
// ウィジェットの作成
// ------------------------------------
// facebook_box
class liquid_widget_fb extends WP_Widget {
	function __construct() {
    	parent::__construct(false, $name = 'Facebook Page Plugin');
    }
    function widget($args, $instance) {
        extract( $args );
        $facebook_ttl = apply_filters( 'widget_facebook_ttl', empty( $instance['facebook_ttl'] ) ? '' : $instance['facebook_ttl'] );
        $facebook_box = apply_filters( 'widget_facebook_box', empty( $instance['facebook_box'] ) ? '' : $instance['facebook_box'] );
    	?>
        <?php echo $before_widget; ?>
        <?php if ( $facebook_ttl ) echo $before_title . $facebook_ttl . $after_title; ?>
        <div class="fb-page" data-href="<?php echo $facebook_box; ?>" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div>
        <?php echo $after_widget; ?>
        <?php
    }
    function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['facebook_ttl'] = trim($new_instance['facebook_ttl']);
	$instance['facebook_box'] = trim($new_instance['facebook_box']);
        return $instance;
    }
    function form($instance) {
        $facebook_ttl = isset( $instance['facebook_ttl'] ) ? esc_attr( $instance['facebook_ttl'] ) : '';
        $facebook_box = isset( $instance['facebook_box'] ) ? esc_attr( $instance['facebook_box'] ) : '';
        ?>
        <p>
           <label for="<?php echo $this->get_field_id('facebook_ttl'); ?>">
           <?php _e( 'Title:', 'liquid-light' ); ?>
           </label>
           <input type="text" class="widefat" rows="16" id="<?php echo $this->get_field_id('facebook_ttl'); ?>" name="<?php echo $this->get_field_name('facebook_ttl'); ?>" value="<?php echo $facebook_ttl; ?>">
        </p>
        <p>
           <label for="<?php echo $this->get_field_id('facebook_box'); ?>">
           <?php _e( 'Facebook Page URL:', 'liquid-light' ); ?>
           </label>
           <input type="text" class="widefat" rows="16" colls="20" id="<?php echo $this->get_field_id('facebook_box'); ?>" name="<?php echo $this->get_field_name('facebook_box'); ?>" value="<?php echo $facebook_box; ?>">
        </p>
        <?php
    }
}
function liquid_widget_fb_register() {
    register_widget( 'liquid_widget_fb' );
}
add_action( 'widgets_init', 'liquid_widget_fb_register' );

// 最近の投稿 (画像付き)
class liquid_widget_newpost extends WP_Widget {

    function __construct() {
        parent::__construct( false, $name = '最近の投稿 (画像付き)' );
    }
    function widget( $args, $instance ) {
        $cache = wp_cache_get( 'widget_recent_posts', 'widget' );
        if ( !is_array( $cache ) ) {
            $cache = array();
        }
        if ( ! isset( $args['widget_id'] ) ) {
            $args['widget_id'] = $this->id;
        }
        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }
        ob_start();
        extract( $args );

        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) ) {
            $number = 10;
        }

        $r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
        if ( $r->have_posts() ) {
        ?>
            <?php echo $before_widget; ?>

            <?php if ( $title ) echo $before_title . $title . $after_title; ?>
            <ul class="newpost">
            <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                <li>
                    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                    <span class="post_thumb"><span><?php echo get_the_post_thumbnail( null, 'thumbnail' ); ?></span></span>
                    <span class="post_ttl"><?php if (get_the_title() ) the_title(); else the_ID(); ?></span></a>
                </li>
            <?php endwhile; ?>
            </ul>
            <?php echo $after_widget; ?>
            <?php
                wp_reset_postdata();
            }
            $cache[ $args['widget_id'] ] = ob_get_flush();
            wp_cache_set( 'widget_recent_posts', $cache, 'widget' );
        }

        function update( $new_instance, $old_instance ) {
            $instance              = $old_instance;
            $instance['title']     = strip_tags($new_instance['title']);
            $instance['number']    = (int) $new_instance['number'];
            //$this->flush_widget_cache();
 
            $alloptions = wp_cache_get( 'alloptions', 'options' );
 
            if ( isset( $alloptions['widget_recent_entries'] ) ) {
                delete_option( 'widget_recent_entries' );
            }
            return $instance;
        }
        //function flush_widget_cache() {
        //    wp_cache_delete( 'widget_recent_posts', 'widget' );
        //}
        /* ウィジェットの設定フォーム */
        function form( $instance ) {
            $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
            $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        ?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'liquid-light' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
 
            <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'liquid-light' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

        <?php
        }
}
function liquid_widget_newpost_register() {
    register_widget( 'liquid_widget_newpost' );
}
add_action( 'widgets_init', 'liquid_widget_newpost_register' );

// ------------------------------------
// LIQUID PRESS functions
// ------------------------------------

//Initialize the update checker.
if ( is_admin() ) {
    //json
    $json_url = "https://lqd.jp/wp/data/_La2XyTTaphfMXqf.json";
    $json = wp_remote_get($json_url);
    if ( is_wp_error( $json ) ) {
       echo '<div class="error"><p>'.$json->get_error_message().'</p></div>';
    }else{
        $json = json_decode($json['body']);
        //update
        liquid_wp_update();
    }
    //json_admin
    $json_admin_url = "https://lqd.jp/wp/data/liquid-light-admin.json";
    $json_admin = wp_remote_get($json_admin_url);
    if ( is_wp_error( $json_admin ) ) {
       //echo '<div class="error"><p>'.$json_admin->get_error_message().'</p></div>';
    }else{
        $json_admin = json_decode($json_admin['body']);
    }
}
function liquid_wp_update(){
    global $json_url;
    require ( get_template_directory() . '/theme-update-checker.php' );
    $liquid_update_checker = new ThemeUpdateChecker(
        'liquid-light',
        $json_url
    );
}

// theme-options
function liquid_theme_support_menu() {
    global $json;
    $ls = '';
    $lqd_options = get_option('lqd_options');
    if (!empty( $lqd_options['ls'] )){
        $ls = htmlspecialchars($lqd_options['ls']);
    }
    echo '<div class="wrap lqd-info"><h1>テーマのサポート</h1>';
    echo '<iframe src="https://lqd.jp/wp/data/liquid-light-info.html?id='.time().'&ls='.$ls.'" frameborder="0" style="width: 100%; height: 1600px;"></iframe>';
    if ( !empty( $ls ) && !empty( $json->version ) ){
        echo '<div class="lqd-footer"><p class="alignleft"><a href="https://lqd.jp/wp/" target="_blank">LIQUID PRESS</a> のご利用ありがとうございます。</p><p class="alignright"><a href="'.$json->download_url.'" target="_blank" class="dlurl">テーマ '.$json->version.' のzipを入手する</a></p></div>';
    }
    echo '</div>';
}
add_action ( 'admin_menu', 'liquid_theme_support' );
function liquid_theme_support() {
     add_theme_page('テーマのサポート', 'テーマのサポート', 'manage_options', 
               'liquid_theme_support', 'liquid_theme_support_menu');
}

// notices
function liquid_admin_notices() {
	global $liquid_theme, $json, $json_admin, $pagenow;
	if ( $pagenow == 'index.php' || $pagenow == 'themes.php' || $pagenow == 'nav-menus.php' ) {
        if( !empty($json_admin->notices) && version_compare($liquid_theme->Version, $json->version, "<") ){
            echo '<div class="notice notice-info"><p>'.$json_admin->notices.'</p></div>';
        }
        if(!empty($json_admin->news)){
            echo '<div class="notice notice-info"><p>'.$json_admin->news.'</p></div>';
        }
    } elseif ( $pagenow == 'edit-tags.php' ) {
        if(!empty($json_admin->catinfo)){
            echo '<div class="notice notice-info"><p>'.$json_admin->catinfo.'</p></div>';
        }
    } elseif ( $pagenow == 'widgets.php' ) {
        if(!empty($json_admin->widgetsinfo)){
            echo '<div class="notice notice-info"><p>'.$json_admin->widgetsinfo.'</p></div>';
        }
    }
}
add_action( 'admin_notices', 'liquid_admin_notices' );

//wpautop
if(!empty($col_options['wpautop'])){
    function liquid_wpautop_action() {
        remove_filter('the_excerpt', 'wpautop');
        remove_filter('the_content', 'wpautop');
    }
    add_filter( 'init', 'liquid_wpautop_action' );
    function liquid_wpautop_filter($init) {
        $init['wpautop'] = false;
        $init['apply_source_formatting'] = ture;
        return $init;
    }
    add_filter( 'tiny_mce_before_init', 'liquid_wpautop_filter' );
}

//sidebar
if ( ! function_exists( 'liquid_col_options' ) ) :
function liquid_col_options($key){
    $col_options = get_option('col_options');
    if(empty($col_options['sidebar'])){
        $mainarea = 'col-md-8';
        $sidebar = 'col-md-4';
    }else{
        $mainarea = 'col-md-12';
        $sidebar = 'col-md-12';
    }
    //
    if($key == 'mainarea'){
        echo $mainarea;
    }else{
        echo $sidebar;
    }
}
endif;

// navigation
if ( ! function_exists( 'liquid_paging_nav' ) ) :
function liquid_paging_nav() {
	global $wp_query, $wp_rewrite;
	if ( $wp_query->max_num_pages < 2 ) {
		return;
	}
	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );
	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}
	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';
	$format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';
	$links = paginate_links( array(
		'base'     => $pagenum_link,
		'format'   => $format,
		'total'    => $wp_query->max_num_pages,
		'current'  => $paged,
		'mid_size' => 4,
		'add_args' => array_map( 'urlencode', $query_args ),
		'prev_text' => __( '&laquo; Prev', 'liquid-light' ),
		'next_text' => __( 'Next &raquo;', 'liquid-light' ),
	) );
	if ( $links ) :
	?>
	<nav class="navigation">
		<ul class="page-numbers">
			<li><?php echo $links; ?></li>
		</ul>
	</nav>
	<?php
	endif;
}
endif;

// load admin file
if (!class_exists('WP_List_Table'))require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
if (!class_exists('WP_Error'))require_once(ABSPATH . 'wp-includes/class-wp-error.php');
require_once(dirname(__FILE__) . '/admin/KeidohShadow_update_db.php' );
require_once(dirname(__FILE__) . '/admin/KeidohShadow_update_db.php' );

function add_KeidohShadow_pages() {
    //DB更新メニュー ==========================
    add_options_page(
        'DB更新（開発用）', // Page Title
        'DB更新（開発用）', // Menu Title
        'administrator', // Capability
        'KeidohShadow_update_db', // Slug
        'KeidohShadow_update_db_page' // Callback
        );
}
add_action('admin_menu', 'add_KeidohShadow_pages');
?>