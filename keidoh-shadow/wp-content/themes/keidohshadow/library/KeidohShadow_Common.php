<?php

/**
 * Pl_Common short summary.
 *
 * Pl_Common description.
 *
 * @version 1.0
 * @author
 */
class SL_Common
{

    public static function CnvStrEmpty($val){
        return $val == null ? "" : $val;
    }
    public static function CnvIntZero($val, $def = 0){
        return $val == null ? $def : intval($val);
    }

    public static function convertEOL($string, $to = "\\\n")
    {
        return preg_replace("/\r\n|\r|\n/", $to, $string);
    }
    //日付の数値から、日付文字列へ変換
    public static function DateIntToYMDStr($dateInt)
    {
        $dateStr = "" . $dateInt;
        if (strlen($dateStr) == 8)
        {
            return substr($dateStr, 0, 4) . '/' . substr($dateStr, 4, 2) .'/' . substr($dateStr, 6, 2);
        }
        return "";
    }
    public static function DateIntToYMDhhmmssStr($dateInt)
    {
        $dateStr = "" . $dateInt;
        if (strlen($dateStr) == 17)
        {
            return substr($dateStr, 0, 4) . '/' . substr($dateStr, 4, 2) .'/' . substr($dateStr, 6, 2)
                . substr($dateStr, 9, 2). substr($dateStr, 12, 2). substr($dateStr, 15, 2);
        }
        return "";
    }
    // table の存在確認
    public static function ExistTable($name)
    {
        global $wpdb; //グローバル変数「$wpdb」を使うよっていう記述
        $wpdb->get_row("SHOW TABLES LIKE '$name'");
        return $wpdb->num_rows == 1;
    }

}