<?php

/**
 * KeidohShadow_Update_Database short summary.
 *
 * KeidohShadow_Update_Database description.
 *
 * @version 1.0
 * @author yuria
 */
class KeidohShadow_Update_Database
{
	public static $PROGRAM_VERSION = 1;

	//更新処理
    public static function DoUpdate(){
        global $wpdb;
        //バージョン取得
        $versioninfo = get_option( 'KeidohShadow_version_info' );//バージョン関係
        $ver = $versioninfo['version'];
        $prefix = $wpdb->prefix;
        $url  = (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"];
        // max id
        if($ver < self::$PROGRAM_VERSION){
            if($ver < 1){
                if(!get_page_by_path("home"))
                {
                    $postIdMax = intval($wpdb->get_var("SELECT MAX(ID) AS postIdMax FROM {$wpdb->posts}"))+1;
                    $userid = wp_get_current_user()->ID;
                    $date = "2018-11-18 12:30:24";
                    $q =
                    "INSERT INTO `${prefix}posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) ".
                    "VALUES(${postIdMax}, ${userid}, '${date}', '${date}', '', 'home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '${date}', '${date}', '', 0, '${$url}/?page_id=${postIdMax}', 0, 'page', '', 0)";
                    $wpdb->query($q);

                    if(!empty($wpdb->last_error)){
                        echo "1-1 更新エラーが発生しました。";
                        echo $wpdb->last_error;
                        return false;
                    }
                }
				if(!get_page_by_path("contact"))
                {
					$postIdMax = intval($wpdb->get_var("SELECT MAX(ID) AS postIdMax FROM {$wpdb->posts}"))+1;
                    $userid = wp_get_current_user()->ID;
                    $date = "2018-11-26 00:00:00";

                    $q =
                    "INSERT INTO `${prefix}posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) ".
                    "VALUES(${postIdMax}, ${userid}, '${date}', '${date}', '', 'contact', '', 'draft', 'closed', 'closed', '', 'contact', '', '', '${date}', '${date}', '', 0, '${$url}/?page_id=${postIdMax}', 0, 'page', '', 0)";
                    $wpdb->query($q);

                    if(!empty($wpdb->last_error)){
                        echo "100-1 更新エラーが発生しました。";
                        echo $wpdb->last_error;
                        return false;
                    }
                }
            }

            $versioninfo = $versioninfo == null ? array() : $versioninfo;
            $versioninfo['version'] = KeidohShadow_Update_Database::$PROGRAM_VERSION;
            $versioninfo['updatedate'] = date_i18n('Y/m/d');
            update_option('KeidohShadow_version_info', $versioninfo);
            //echo "データベースの更新が完了しました。";
            return true;
        }

        //echo 'データベースの更新はありませんでした。';
        return false;
    }
}