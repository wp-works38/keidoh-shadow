           <div class="<?php liquid_col_options('sidebar'); ?> sidebar">
             <div class="widgets">
                <?php if(! dynamic_sidebar('sidebar')){ ?>
                 <!-- no widget -->
                 <div class="widget search">
                  <?php get_search_form(); ?>
                 </div>
                 <div class="widget cats">
                  <div class="ttl"><?php esc_html_e( 'Categories', 'liquid-light' ); ?></div>
                  <ul class="list-unstyled">
                    <?php wp_list_categories('title_li='); ?>
                  </ul>
                 </div>
                 <div class="widget tags">
                  <div class="ttl"><?php esc_html_e( 'Tags', 'liquid-light' ); ?></div>
                  <ul class="list-unstyled">
                    <?php
                    $args = array(
                        'taxonomy' => 'post_tag',
                        'title_li' => ''
                    );
                    wp_list_categories( $args );
                    ?>
                  </ul>
                 </div>
                <?php } ?>
             </div>
           </div>