<?php get_header(); ?>
   
    <div <?php post_class('detail'); ?>>
        <div class="container">
          <div class="row">
           <div class="<?php liquid_col_options('mainarea'); ?> mainarea">

           <?php if (have_posts()) : ?>
           <?php while (have_posts()) : the_post(); ?>
           
            <h1 class="ttl_h1 entry-title"><?php the_title(); ?></h1>
            
            <!-- pan -->
            <?php $cat = get_the_category(); ?>
            <nav aria-label="breadcrumb">
             <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
              <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="<?php echo esc_url( home_url() ); ?>" itemprop="item"><span itemprop="name"><?php esc_html_e( 'TOP', 'liquid-light' ); ?></span></a><meta itemprop="position" content="1"></li>
              <?php $item_position = 1; ?>
              <?php if( !empty($cat) && !empty($cat[0]->parent) ){ ?>
              <?php $item_position++; ?>
              <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="<?php echo get_category_link($cat[0]->parent); ?>" itemprop="item"><span itemprop="name"><?php echo get_cat_name($cat[0]->parent); ?></span></a><meta itemprop="position" content="<?php echo $item_position; ?>"></li>
              <?php } ?>
              <?php if( !empty($cat) && !empty($cat[0]->term_id) ){ ?>
              <?php $item_position++; ?>
              <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="<?php echo get_category_link($cat[0]->term_id); ?>" itemprop="item"><span itemprop="name"><?php echo get_cat_name($cat[0]->term_id); ?></span></a><meta itemprop="position" content="<?php echo $item_position; ?>"></li>
              <?php } ?>
              <?php $item_position++; ?>
              <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" aria-current="page"><a title="<?php the_title(); ?>" itemprop="item"><span itemprop="name"><?php the_title(); ?></span></a><meta itemprop="position" content="<?php echo $item_position; ?>"></li>
             </ul>
            </nav>
           
            <div class="detail_text">

               <?php get_template_part('share');?>

                <div class="post_meta">
                <span class="post_time">
                 <i class="icon icon-clock" title="<?php echo get_the_time("Y/m/d H:i"); ?>"></i><?php if ( get_the_date() != get_the_modified_date() ) : ?> <i class="icon icon-spinner11" title="<?php echo get_the_modified_date("Y/m/d H:i"); ?>"></i><?php endif; ?> <time class="date updated"><?php echo get_the_date(); ?></time>
                </span>
                <?php if($cat){ ?>
                    <span class="post_cat"><i class="icon icon-folder"></i>
                    <?php the_category(', '); ?>
                    </span>
                <?php } ?>
                </div>
                <?php if(has_post_thumbnail()) { the_post_thumbnail(); } ?>
                <?php if(! dynamic_sidebar('main_head')): ?><!-- no widget --><?php endif; ?>
                <div class="post_body"><?php the_content(); ?></div>
                <?php
                // ページング
                $args = array(
                    'before' => '<nav><ul class="page-numbers">', 
                    'after' => '</ul></nav>', 
                    'link_before' => '<li>', 
                    'link_after' => '</li>'
                );
                wp_link_pages( $args );
                ?>
                <?php if(! dynamic_sidebar('main_foot')): ?><!-- no widget --><?php endif; ?>
                <?php the_tags( '<ul class="list-inline tag"><li class="list-inline-item">', '</li><li class="list-inline-item">', '</li></ul>' ); ?>
            </div>
            <?php endwhile; ?>
            <div class="detail_comments">
                <?php comments_template(); ?>
            </div>
            <?php else : ?>
                <p><?php esc_html_e( 'No articles', 'liquid-light' ); ?></p>
                <?php get_search_form(); ?>
            <?php endif; ?>
           
            <nav>
              <ul class="pagination justify-content-between">
                <?php
                $prev_post = get_previous_post();
                $next_post = get_next_post();
                if (!empty( $prev_post )) {
                    echo '<li><a href="'.get_permalink( $prev_post->ID ).'" class="badge-pill" title="'.htmlspecialchars($prev_post->post_title).'">'.esc_html__( '&laquo; Prev', 'liquid-light' ).'</a></li>';
                }
                //if (!empty( $cat )) {
                //    echo '<li>';
                //    the_category('</li><li>');
                //    echo '</li>';
                //}
                if (!empty( $next_post )) {
                    echo '<li><a href="'.get_permalink( $next_post->ID ).'" class="badge-pill" title="'.htmlspecialchars($next_post->post_title).'">'.esc_html__( 'Next &raquo;', 'liquid-light' ).'</a></li>';
                } ?>
                </ul>
            </nav>

            <?php //recommend
            $original_post = $post;
            $tags = wp_get_post_tags($post->ID);
            $tagIDs = array();
            if ($tags) {
              $tagcount = count($tags);
              for ($i = 0; $i < $tagcount; $i++) {
                  $tagIDs[$i] = $tags[$i]->term_id;
              }
              $args=array(
              'tag__in' => $tagIDs,
              'post__not_in' => array($post->ID),
              'posts_per_page' => 4,
              'ignore_sticky_posts' => 1
              );
            }elseif($cat){
              $args=array(
              'cat' => $cat[0]->cat_ID,
              'post__not_in' => array($post->ID),
              'posts_per_page' => 4,
              'ignore_sticky_posts' => 1
              );
            }else{
              $args=array(
              'post__not_in' => array($post->ID),
              'posts_per_page' => 4,
              'ignore_sticky_posts' => 1
              );
            }
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) { ?>
            <div class="recommend">
              <div class="ttl"><?php esc_html_e( 'Recommend', 'liquid-light' ); ?></div>
              <div class="row">
                <?php 
                while ($my_query->have_posts()) : $my_query->the_post();
                //thumb
                $src = "";
                if(has_post_thumbnail($post->ID)){
                    // アイキャッチ画像を設定済みの場合
                    $thumbnail_id = get_post_thumbnail_id($post->ID);
                    $src_info = wp_get_attachment_image_src($thumbnail_id, 'large');
                    $src = $src_info[0];
                }else{
                    // アイキャッチが設定されていない場合
                    if(preg_match('/<img([ ]+)([^>]*)src\=["|\']([^"|^\']+)["|\']([^>]*)>/',$post->post_content,$img_array)){
                        $src = $img_array[3];
                    }else{
                        $src = get_stylesheet_directory_uri().'/images/noimage.png';
                    }
                }
                ?>
                <article class="card col-md-6">
                  <div class="card-block">
                   <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="post_links">
                   <span class="post_thumb" style="background-image: url('<?php echo $src; ?>')"></span>
                   <span class="card-text">
                       <span class="post_time"><i class="icon icon-clock"></i> <?php echo get_the_date(); ?></span>
                   </span>
                   <h3 class="card-title post_ttl"><?php the_title(); ?></h3>
                   </a>
                  </div>
                </article>
                <?php endwhile; wp_reset_query(); ?>
              </div>
            </div>
            <?php } else { ?>
            <!-- <?php esc_html_e( 'No articles', 'liquid-light' ); ?> -->
            <?php } ?>
            
           </div><!-- /col -->
           <?php get_sidebar(); ?>
           
         </div><!-- /row -->
        </div><!-- /container -->
    </div><!-- /detail -->

<?php get_footer(); ?>